const theme = {
  colors: {
    lightBlue: 'linear-gradient(#6d88f3, #48fbdb)',
    buttonColor: '#8400ff',
    white: '#FFF',
  },
  fonts: 'arial',
  fontSizes: {
    small: '1em',
    medium: '2em',
    large: '3em',
  },
};
export default theme;
