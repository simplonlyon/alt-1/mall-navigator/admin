import { IShop } from './IShop';

export interface IPropsShop {
  data: IShop;
}
