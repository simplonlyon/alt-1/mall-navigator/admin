export interface IShop {
  Id: number;
  ShopName: string;
  Floor: number;
  Description: string;
  start: string;
  end: string;
  Number: number;
  Img: string;
  Long: number;
  Lat: number;
  Link: string;
}
