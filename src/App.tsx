import React from 'react';
import { HashRouter as Router, Switch, Route, Link } from 'react-router-dom';
import { ThemeProvider } from 'styled-components';
import AddShop from './components/AddShop';
import CreateUserScreen from './components/CreateUser';
import LogScreen from './components/LogScreen';
import ShopList from './components/ShopList';
import NavBar from './components/navBar';
import theme from './utils/theme';

const Hello = () => {
  return (
    <div>
      <h1>Welcome to Shopify</h1>
      <Link to="/addshop">Go to Add A Shop</Link>
      <Link to="/logScreen">Test login</Link>
      <Link to="/ShopList">!Go to Add A Shop List!</Link>
      <Link to="/App">Test Navbar</Link>
    </div>
  );
};

export default function App() {
  return (
    <>
      <ThemeProvider theme={theme}>
        <Router>
          <Switch>
            <Route path="/App" component={NavBar} />
            <Route path="/logScreen" component={LogScreen} />
            <Route path="/logScreen" component={LogScreen} />
            <Route path="/createAccount" component={CreateUserScreen} />
            <Route path="/addshop" component={AddShop} />
            <Route path="/ShopList" component={ShopList} />
            <Route path="/" component={Hello} />
          </Switch>
        </Router>
      </ThemeProvider>
    </>
  );
}
