/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react';
import styled from 'styled-components';

interface GroupFormProps {
  label?: string;
  error?: boolean | string;
  placeholder?: string;
}

const GroupForm: React.FC<GroupFormProps> = ({
  label,
  error,
  placeholder,
  ...rest
}) => {
  return (
    <>
      <Group>
        <Label>{label}</Label>
        <Input type="text" {...rest} placeholder={placeholder} />
      </Group>
      <ValidationContainer placeholder={placeholder}>
        <Validation>{error}</Validation>
      </ValidationContainer>
    </>
  );
};
const Group = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  flex-direction: row;
`;
const Input = styled.input`
  font-family: arial;
  border: none;
  align-items: center;
  margin-top: 10px;
  outline: none;
  background-color: transparent;
  border-bottom: 1px solid black;
`;
const Label = styled.text`
  font-family: arial;
  font-weight: bold;
  color: black;
  margin-top: 10px;
  margin-right: 10px;
`;
const ValidationContainer = styled.div`
  flex-direction: row;
  display: flex;
  justify-content: flex-end;
`;
const Validation = styled.text`
  color: red;
  margin-top: 10px;
  font-size: 10px;
`;

export default GroupForm;
