/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';

function Details({ data, closeModel }: any) {
  return (
    <Container>
      <ModalBody className="modal-main">
        <CloseButtonContainer>
          <CloseButton type="button" onClick={closeModel}>
            X
          </CloseButton>
        </CloseButtonContainer>
        <ContentContainer>
          <ImgContainer>
            <img src={data.Img} width="100px" height="100px" alt="" />
          </ImgContainer>
          <Description>
            <Title>TODO: Finir la modal</Title>
            <TimeLabel>
              {data.start} - {data.end}
            </TimeLabel>
            <DescriptionLabel>Floor: {data.Floor}</DescriptionLabel>
            <DescriptionLabel>{data.Description}</DescriptionLabel>
          </Description>
        </ContentContainer>
        <ButtonContainer>
          <EditButton
            to={{
              pathname: '/addshop',
              state: data,
            }}
          >
            Edit
          </EditButton>
          <DeleteButton to="/">Delete</DeleteButton>
        </ButtonContainer>
      </ModalBody>
    </Container>
  );
}

export default Details;

const Container = styled.div`
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background: rgba(0, 0, 0, 0.6);
`;

const ModalBody = styled.div`
  position: fixed;
  background: white;
  width: 45%;
  height: auto;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`;

const ImgContainer = styled.div`
  height: 100%;
  width: 25%;
  align-items: center;
  display: flex;
  justify-content: center;
`;

const ContentContainer = styled.div`
  height: 100%;
  display: flex;
  width: 100%;
`;

const Title = styled.p`
  font-size: 20px;
  font-weight: bold;
  margin: 0px;
`;

const TimeLabel = styled.p`
  margin: 0px;
`;

const Description = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;

const DescriptionLabel = styled.p`
  margin: 0px;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  padding: 5px;
  align-items: center;
  flex-wrap: wrap;
`;

const EditButton = styled(Link)`
  border-radius: 10px;
  background-color: #2ab2b2;
  text-align: center;
  text-decoration: none;
  color: white;
  width: 50px;
  margin-right: 5px;
`;

const DeleteButton = styled(Link)`
  border-radius: 10px;
  background-color: red;
  text-align: center;
  text-decoration: none;
  color: white;
  width: 65px;
  margin-left: 5px;
`;

const CloseButtonContainer = styled.div`
  display: flex;
  flex-direction: row-reverse;
  padding: 5px;
`;

const CloseButton = styled.button`
  border-radius: 10px;
  background-color: #ff000000;
  text-align: center;
  text-decoration: none;
  color: black;
  width: 25px;
`;
