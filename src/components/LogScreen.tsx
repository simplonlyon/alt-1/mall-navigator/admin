/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react';
import { useForm } from 'react-hook-form';
import { Link, useHistory } from 'react-router-dom';
import styled from 'styled-components';

interface IRegister {
  username: string;
  password: string;
}

const userList = [
  ['Clement', 'abcde'],
  ['Lucas', 'fghij'],
  ['Sarah', 'klmno'],
];

function LogScreen() {
  const { handleSubmit, register, getValues, setValue } = useForm<IRegister>();
  const history = useHistory();
  let isLogged = false;

  const submitLogin = () => {
    const { username, password } = getValues();
    userList.forEach((element) => {
      if (element[0] === username && element[1] === password) {
        alert('login accepted');
        history.push('/');
        isLogged = true;
      }
    });
    if (!isLogged) {
      setValue('password', '');
      setValue('username', '');
      alert('login Failed');
    }
  };
  return (
    <Container>
      <form onSubmit={submitLogin}>
        <div className="topBar">
          <Input
            type="text"
            placeholder="Username"
            autoComplete="username"
            {...register('username')}
          />
          <InputPassword
            type="password"
            placeholder="Password"
            autoComplete="current-password"
            {...register('password')}
          />
        </div>
        <InputSubmit type="submit" value="Login" />
      </form>
      <NavBtn to="/createAccount">Not registered ? Create an account</NavBtn>
      <Title>Mole Navigation</Title>
    </Container>
  );
}

const Container = styled.div`
  background-size: cover;
  width: 1905px;
  height: 960px;
  margin-left: -8px;
  margin-top: -8px;
  background-image: url(../assets/gradient_back.png);
`;

const Input = styled.input`
  position: absolute;
  margin-top: 400px;
  margin-left: 775px;
  height: 35px;
  width: 350px;
`;

const InputPassword = styled.input`
  position: absolute;
  margin-top: 460;
  margin-left: 775;
  height: 35px;
  width: 350px;
`;

const InputSubmit = styled.input`
  position: absolute;
  margin-top: 575px;
  margin-left: 865px;
  height: 50px;
  width: 170px;
  border-radius: 68px;
  background-color: #8400ff;
  color: white;
  font-weight: bold;
  font-size: 20px;
`;

const NavBtn = styled(Link)`
  position: absolute;
  margin-top: 515px;
  margin-left: 835px;
`;

const Title = styled.h1`
  position: absolute;
  color: white;
  margin-left: 835px;
  margin-top: 250px;
`;

export default LogScreen;
