/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import Element from './Element';
import LogoImg from '../../assets/icons/shop.png';

function ShopList() {
  const test = [
    {
      Id: 1,
      ShopName: 'test1',
      Floor: 3,
      Description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis gravida nunc, id mollis nunc.',
      start: '8:00',
      end: '17:00',
      Number: 5,
      Img: LogoImg,
      Long: 39.063136,
      Lat: 125.789152,
      Link: 'http://www.republiquedesmangues.fr',
    },
    {
      Id: 2,
      ShopName: 'test2',
      Floor: 5,
      Description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis gravida nunc, id mollis nunc.',
      start: '9:00',
      end: '16:00',
      Number: 7,
      Img: LogoImg,
      Long: 39.032103,
      Lat: 125.753272,
      Link: 'http://endless.horse',
    },
    {
      Id: 3,
      ShopName: 'test3',
      Floor: 1,
      Description:
        'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus quis gravida nunc, id mollis nunc.',
      start: '7:00',
      end: '15:00',
      Number: 1,
      Img: LogoImg,
      Long: 55.754405,
      Lat: 37.619698,
      Link: 'https://www.kreml.ru',
    },
  ];

  return (
    <Container>
      <AddButtonContainer>
        <AddButton to="/addshop">Add a shop</AddButton>
      </AddButtonContainer>
      <Scroll>
        <List>
          {test.map((item) => {
            return (
              <Content key={item.Id}>
                <Element data={item} />
              </Content>
            );
          })}
        </List>
      </Scroll>
    </Container>
  );
}

const Content = styled.li`
  background-color: #ffff;
  list-style-type: none;
  margin: 10px;
  box-shadow: 2px 3px 10px 0px #00000085;
  height: 100px;
  padding: 5px;
`;

const Container = styled.div`
  background-color: #ffff;
  height: 100vh;
`;

const Scroll = styled.div`
  overflow-y: auto;
  height: 350px;
`;

const List = styled.ul`
  width: 75%;
  margin-left: auto;
  margin-right: auto;
`;

const AddButtonContainer = styled.div`
  width: 75%;
  margin-left: auto;
  margin-right: auto;
  display: flex;
  flex-direction: row-reverse;
  margin-bottom: 10px;
`;

const AddButton = styled(Link)`
  border-radius: 10px;
  background-color: #8400ff;
  text-align: center;
  text-decoration: none;
  color: white;
  width: 100px;
`;

export default ShopList;
