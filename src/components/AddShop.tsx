/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useState } from 'react';
import { SubmitHandler, useForm } from 'react-hook-form';
import { Link, useLocation } from 'react-router-dom';
import styled from 'styled-components';
import GroupForm from '../shared/GroupForm';
import LogoImg from '../../assets/icons/shop.png';

interface IFormInput {
  shopName: string;
  description: string;
  opening: string;
  closing: string;
  long: string;
  lat: string;
  link: string;
}

// interface ModalProps {
//   modal: boolean;
// }

const AddShop = () => {
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<IFormInput>();
  const { state } = useLocation();
  console.log(state);
  const onSubmit: SubmitHandler<IFormInput> = (data) => {
    console.log(data);
    if (data) {
      alert('Successfully added shop');
    }
  };

  return (
    <>
      <Container>
        <img alt="shop-logo" src={LogoImg} width={50} height={50} />
        <Title>Add Shop to Your List</Title>
        <FormContainer>
          <Form onSubmit={handleSubmit(onSubmit)}>
            <GroupForm
              label="Shop Name"
              placeholder="La Fnac"
              {...register('shopName', { required: true })}
              defaultValue={state?.ShopName}
              error={errors.shopName && 'shopname is required'}
            />
            <GroupForm
              label="Description"
              placeholder="Coffee shop"
              defaultValue={state?.Description}
              {...register('description', { required: true })}
              error={errors.description && 'description is required'}
            />
            <GroupForm
              label="Opening at"
              {...register('opening')}
              placeholder="9h45"
              defaultValue={state?.start}
            />
            <GroupForm
              label="Closing at"
              placeholder="20h45"
              defaultValue={state?.end}
              {...register('closing')}
            />

            <GroupForm
              label="Long"
              placeholder="45.7569"
              defaultValue={state?.Long}
              {...register('long', { required: true })}
              error={errors.long && 'longitude should be a number'}
            />

            <GroupForm
              label="Lat"
              defaultValue={state?.Lat}
              {...register('lat', { required: true })}
              placeholder="45.675869"
              error={errors.long && 'latitude should be a number'}
            />

            <GroupForm
              label="Link internet"
              {...register('link')}
              defaultValue={state?.Link}
            />
            <Button type="submit">Add to List +</Button>
          </Form>
        </FormContainer>
        <StyledLink to="/">Back to HomeScreen</StyledLink>
      </Container>
    </>
  );
};

const Container = styled.div`
  background: ${(p) => p.theme.colors.lightBlue};
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  height: 100vh;
`;
const FormContainer = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
`;

const Title = styled.h1`
  color: ${(p) => p.theme.colors.white};
  font-size: ${(p) => p.theme.fontSizes.medium};
  font-family: ${(p) => p.theme.fonts};
  text-align: center;
`;
const Form = styled.form`
  font-family: ${(p) => p.theme.fonts};
  width: 300px;
`;
const Button = styled.button`
  margin-top: 50px;
  font-family: ${(p) => p.theme.fonts};
  font-weight: bold;
  color: white;
  cursor: pointer;
  border-radius: 50px;
  margin-right: 10px;
  border: none;
  height: 30px;
  width: 200px;
  outline: none;
  background-color: ${(p) => p.theme.colors.buttonColor};
  &:hover {
    background-color: blue;
    color: ${(p) => p.theme.colors.white};
  }
`;
const StyledLink = styled(Link)`
  text-decoration: none;
  margin-top: 30px;
  font-family: ${(p) => p.theme.fonts};
  color: ${(p) => p.theme.colors.white};
  &:focus,
  &:hover,
  &:visited,
  &:link,
  &:active {
    text-decoration: none;
  }
`;

export default AddShop;
