/* eslint-disable @typescript-eslint/naming-convention */
/* eslint-disable react/no-unused-prop-types */
/* eslint-disable @typescript-eslint/no-use-before-define */
import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import styled from 'styled-components';
import { IPropsShop } from '../interfaces/IPropsShop';
import Details from '../shared/Details';

function Element({ data }: IPropsShop) {
  const [show, setShow] = useState(false);

  const toggleModal = () => setShow(!show);

  return (
    <>
      <ContentContainer onClick={toggleModal}>
        <ImgContainer>
          <img src={data.Img} width="50px" height="50px" alt="" />
        </ImgContainer>
        <Test>
          <Title>{data.ShopName}</Title>
          <TimeLabel>
            {data.start} - {data.end}
          </TimeLabel>
        </Test>
        <Description>
          <DescriptionLabel>Floor: {data.Floor}</DescriptionLabel>
          <DescriptionLabel>{data.Description}</DescriptionLabel>
        </Description>
        <ButtonContainer>
          <EditButton
            to={{
              pathname: '/addshop',
              state: data,
            }}
          >
            Edit
          </EditButton>
          <DeleteButton to="/">Delete</DeleteButton>
        </ButtonContainer>
      </ContentContainer>
      {show && <Details data={data} closeModel={toggleModal} />}
    </>
  );
}

export default Element;

const ImgContainer = styled.div`
  height: 100%;
  width: 10%;
  align-items: center;
  display: flex;
  justify-content: center;
`;

const ContentContainer = styled.div`
  height: 100%;
  display: flex;
  width: 100%;
`;

const Test = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: center;
  padding: 5px;
  width: 10%;
`;

const Title = styled.p`
  font-size: 20px;
  font-weight: bold;
  margin: 0px;
`;

const TimeLabel = styled.p`
  margin: 0px;
`;

const Description = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  justify-content: center;
  text-align: center;
`;

const DescriptionLabel = styled.p`
  margin: 0px;
`;

const ButtonContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  width: 12%;
  padding: 5px;
  align-items: center;
  flex-wrap: wrap;
`;

const EditButton = styled(Link)`
  border-radius: 10px;
  background-color: #2ab2b2;
  text-align: center;
  text-decoration: none;
  color: white;
  width: 50px;
`;

const DeleteButton = styled(Link)`
  border-radius: 10px;
  background-color: red;
  text-align: center;
  text-decoration: none;
  color: white;
  width: 65px;
`;
