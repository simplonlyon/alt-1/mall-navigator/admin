/* eslint-disable react/button-has-type */
/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';

function NavBar() {
  const history = useHistory();

  return (
    <Container>
      <Button
        onClick={() => {
          history.push('addshop');
        }}
      >
        ADD A SHOP
      </Button>
      <Disconnect
        onClick={() => {
          history.push('logScreen');
        }}
      >
        Disconnect
      </Disconnect>
    </Container>
  );
}

const Container = styled.div`
  position: 'absolute';
  width: 1905;
  height: 200;
  background-image: 'url(../assets/gradient_back.png)';
`;

const Button = styled.button`
  font-size: 30;
  color: 'white';
  margin-left: 850;
  margin-top: 150;
  background-color: 'rgba(52, 52, 52, 0)';
  border: 'none';
  position: 'absolute';
`;
const Disconnect = styled.button`
  font-size: 20;
  color: 'white';
  margin-left: 1700;
  margin-top: 150;
  background-color: 'rgba(52, 52, 52, 0)';
  border: 'none';
  position: 'absolute';
`;

export default NavBar;
