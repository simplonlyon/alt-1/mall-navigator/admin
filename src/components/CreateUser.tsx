/* eslint-disable @typescript-eslint/no-use-before-define */
import React from 'react';
import { useForm } from 'react-hook-form';
import { Link, useHistory } from 'react-router-dom';
import styled from 'styled-components';

interface IRegister {
  username: string;
  password: string;
}

// var userList = [["Clement", "abcde"], ["Lucas", "fghij"], ["Sarah", "klmno"]];
function CreateUserScreen() {
  const { handleSubmit, register, getValues } = useForm<IRegister>();
  const history = useHistory();

  const submitLogin = () => {
    const { password, username } = getValues();
    if (password !== '' && username !== '') {
      alert('User created succesfully');
      history.push('/logScreen');
    } else {
      alert('Password and username cannot be empty');
    }
  };

  return (
    <Container>
      <form onSubmit={handleSubmit(submitLogin)}>
        <div className="topBar">
          <InputText
            type="text"
            placeholder="Username"
            autoComplete="username"
            {...register('username')}
          />
          <InputPassword
            type="password"
            placeholder="Password"
            autoComplete="current-password"
            {...register('password')}
          />
        </div>
        <InputSubmit type="submit" value="Create account" />
      </form>
      <NavBtn to="/logScreen">Already registered ? Sign in</NavBtn>
      <Title>Mole Navigation</Title>
    </Container>
  );
}

const Container = styled.div`
  background-size: cover;
  width: 1905px;
  height: 960px;
  margin-left: -8px;
  margin-top: -8px;
  background-image: url(../assets/gradient_back.png);
`;

const InputText = styled.input`
  position: absolute;
  margin-top: 400px;
  margin-left: 775px;
  height: 35px;
  width: 350px;
`;

const InputPassword = styled.input`
  position: absolute;
  margin-top: 460px;
  margin-left: 775px;
  height: 35px;
  width: 350px;
`;

const InputSubmit = styled.input`
  position: absolute;
  margin-top: 575px;
  margin-left: 865px;
  height: 50px;
  width: 170px;
  border-radius: 68px;
  background-color: #8400ff;
  color: white;
  font-weight: bold;
  font-size: 20px;
`;

const NavBtn = styled(Link)`
  position: absolute;
  margin-top: 515px;
  margin-left: 855px;
`;

const Title = styled.h1`
  position: absolute;
  color: white;
  margin-left: 835px;
  margin-top: 250px;
`;

export default CreateUserScreen;
