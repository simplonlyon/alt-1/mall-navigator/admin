## Admin

`yarn install`

- Reste à faire :

  - Connecter les méthodes de l'API

- AddShop.tsx

  - fetch POST pour ajouter le shop dans la méthode
    on submit

- ShopList
  - fetch GET All Shop
  - fetch GET One Shop
  - Au click du bouton "edit" => redirection sur AddShop avec l'id et le formulaire pré-rempli qui vient de la liste (Fetch POST shops/1) conditionnel "EditMode".

-LogScreen

- fetch POST pour ajouter un user en tant qu'admin

Lien Figma : https://www.figma.com/file/CvSJS5x7k6Hr6Jq5d5njv7/symfoApp?node-id=0%3A1
